package com.thuha.lesson4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainAct extends BaseAct {
    public static final String KEY_IMAGE = "KEY_IMAGE";
    public static final String KEY_TITIE = "KEY_TITLE";
    public static final String KEY_DETAIL = "KEY_DETAIL";
    public static final String KEY_ZODIAC = "KEY_ZODIAC";
    private ImageView imgNhanMa, imgSuTu, imgBaoBinh, imgBachDuong, imgBoCap, imgCuGiai, imgKimNguu, imgMaKet, imgSongNgu, imgSongTu, imgThienBinh, imgXuNu, imgMini;
    private Button btXemThem;
    private TextView tvTitle, tvDetail;
    private String name;
    private List<Zodiac> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }

    private void initData() {
        list.add(new Zodiac(R.id.bachduong, getResources().getString(R.string.bach_duong_title2), getResources().getString(R.string.bach_duong_text), R.drawable.ic_bach_duong));
        list.add(new Zodiac(R.id.xunu, getResources().getString(R.string.xu_nu_title2), getResources().getString(R.string.xu_nu_text), R.drawable.ic_xu_nu));
        list.add(new Zodiac(R.id.baobinh, getResources().getString(R.string.bao_binh_title2), getResources().getString(R.string.bao_binh_text), R.drawable.ic_bao_binh));
        list.add(new Zodiac(R.id.bocap, getResources().getString(R.string.bo_cap_title2), getResources().getString(R.string.bo_cap_text), R.drawable.ic_bocap));
        list.add(new Zodiac(R.id.cugiai, getResources().getString(R.string.cu_giai_title2), getResources().getString(R.string.cu_giai_text), R.drawable.ic_cu_giai));
        list.add(new Zodiac(R.id.maket, getResources().getString(R.string.ma_ket_title2), getResources().getString(R.string.ma_ket_text), R.drawable.ic_ma_ket));
        list.add(new Zodiac(R.id.nhanma, getResources().getString(R.string.nhan_ma_title2), getResources().getString(R.string.nhan_ma_text), R.drawable.ic_nhan_ma));
        list.add(new Zodiac(R.id.songngu, getResources().getString(R.string.song_ngu_title2), getResources().getString(R.string.song_ngu_text), R.drawable.ic_song_ngu));
        list.add(new Zodiac(R.id.songtu, getResources().getString(R.string.song_tu_title2), getResources().getString(R.string.song_tu_text), R.drawable.ic_song_tu));
        list.add(new Zodiac(R.id.sutu, getResources().getString(R.string.su_tu_title2), getResources().getString(R.string.su_tu_text), R.drawable.ic_su_tu));
        list.add(new Zodiac(R.id.thienbinh, getResources().getString(R.string.thien_binh_title2), getResources().getString(R.string.thien_binh_text), R.drawable.ic_thien_binh));
        list.add(new Zodiac(R.id.kimnguu, getResources().getString(R.string.kim_nguu_title2), getResources().getString(R.string.kim_nguu_text), R.drawable.ic_kim_nguu));
        zodiac = findZodiac(R.id.bachduong);
        updateUI();
    }

    private void initView() {
        list = new ArrayList<>();

        imgNhanMa = findViewById(R.id.nhanma);
        imgNhanMa.setOnClickListener(this);

        imgSuTu = findViewById(R.id.sutu);
        imgSuTu.setOnClickListener(this);

        imgBaoBinh = findViewById(R.id.baobinh);
        imgBaoBinh.setOnClickListener(this);

        imgBachDuong = findViewById(R.id.bachduong);
        imgBachDuong.setOnClickListener(this);

        imgBoCap = findViewById(R.id.bocap);
        imgBoCap.setOnClickListener(this);

        imgCuGiai = findViewById(R.id.cugiai);
        imgCuGiai.setOnClickListener(this);

        imgKimNguu = findViewById(R.id.kimnguu);
        imgKimNguu.setOnClickListener(this);

        imgMaKet = findViewById(R.id.maket);
        imgMaKet.setOnClickListener(this);

        imgSongNgu = findViewById(R.id.songngu);
        imgSongNgu.setOnClickListener(this);

        imgSongTu = findViewById(R.id.songtu);
        imgSongTu.setOnClickListener(this);

        imgThienBinh = findViewById(R.id.thienbinh);
        imgThienBinh.setOnClickListener(this);

        imgXuNu = findViewById(R.id.xunu);
        imgXuNu.setOnClickListener(this);

        imgMini = findViewById(R.id.imv_minipanel);

        tvTitle = findViewById(R.id.tv_title);
        tvDetail = findViewById(R.id.tv_detail);

        btXemThem = findViewById(R.id.bt_add);
        btXemThem.setOnClickListener(this);

    }

    private Zodiac zodiac;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.xunu:
                zodiac = findZodiac(R.id.xunu);
                break;
            case R.id.nhanma:
                zodiac = findZodiac(R.id.nhanma);
                break;
            case R.id.baobinh:
                zodiac = findZodiac(R.id.baobinh);
                break;
            case R.id.bocap:
                zodiac = findZodiac(R.id.bocap);
                break;
            case R.id.sutu:
                zodiac = findZodiac(R.id.sutu);
                break;
            case R.id.bachduong:
                zodiac = findZodiac(R.id.bachduong);
                break;
            case R.id.cugiai:
                zodiac = findZodiac(R.id.cugiai);
                break;
            case R.id.kimnguu:
                zodiac = findZodiac(R.id.kimnguu);
                break;
            case R.id.maket:
                zodiac = findZodiac(R.id.maket);
                break;
            case R.id.songngu:
                zodiac = findZodiac(R.id.songngu);
                break;
            case R.id.songtu:
                zodiac = findZodiac(R.id.songtu);
                break;
            case R.id.thienbinh:
                zodiac = findZodiac(R.id.thienbinh);
                break;
            default:
                break;
            case R.id.bt_add:
                onpenDetailAct();
        }
        updateUI();
    }

    private void updateUI() {
        imgMini.setImageResource(zodiac.getImage());
        tvTitle.setText(zodiac.getTitle());
        tvDetail.setText(zodiac.getDetail());
    }

    private Zodiac findZodiac(int id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == id) {
                return list.get(i);
            }
        }
        return null;
    }

    private void onpenDetailAct() {
        Intent intent = new Intent();
        intent.setClass(this, DetailAct.class);
        intent.putExtra(KEY_ZODIAC, zodiac);
        startActivity(intent);
    }

}
