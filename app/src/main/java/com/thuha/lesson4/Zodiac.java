package com.thuha.lesson4;

import java.io.Serializable;

public class Zodiac implements Serializable {
    private int id;
    private String  title, detail;
    private int image;

    public Zodiac(int id, String title, String detail, int image) {
        this.id = id;
        this.title = title;
        this.detail = detail;
        this.image = image;
    }

    public int getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }

    public int getImage() {
        return image;
    }
}
