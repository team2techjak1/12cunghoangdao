package com.thuha.lesson4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailAct extends BaseAct {
    private ImageView img;
    private TextView tvTitle, tvDetail;
    private Button btBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_game_detail);
        initView();
        initData();

    }

    private void initData() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        Zodiac zo = (Zodiac) intent.getSerializableExtra(MainAct.KEY_ZODIAC);
        img.setImageResource(zo.getImage());
        String title = zo.getTitle();
        title = title.replace("(", "\n(");
        tvTitle.setText(title);
        tvDetail.setText(zo.getDetail());
    }

    private void initView() {
        img = findViewById(R.id.imv);
        tvTitle = findViewById(R.id.tv_title);
        tvDetail = findViewById(R.id.tv_detail);
        btBack = findViewById(R.id.bt_back);
        btBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.bt_back){
            finish();
        }
    }
}
